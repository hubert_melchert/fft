#include <stdint.h>
#include <stddef.h>


//USER DEFINED PARAMETERS
#define BIT_PRECISION (12)
#define SAMPLES  (16)

typedef struct
{
	int32_t re;
	int32_t im;
}complex_t;

void FFT_init(void);
uint16_t FFT_complexAbs(complex_t *input);
complex_t *FFT_complexFFT(complex_t *const data, size_t size);
complex_t *FFT_bitReverseArray(uint16_t *inputArray, complex_t *outputArray, 
                               uint16_t size);
void FFT_generateTwiddleFactors(void);
