#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include "fft.h"

#define CLEAR_BIT(bitNum, val) if(1){val &= ~(1 << bitNum);}
#define IS_POWER_OF_2(x) ((x != 0) && ((x & (x-1)) == 0))
#ifndef NELEMS
#define NELEMS(array) (sizeof(array) / sizeof(array[0]))
#endif

#ifndef ASSERT
#define CONCAT(a,b) a##b
#define __ASSERT(predicate, line) \
            typedef char CONCAT(assertion_failed, line)[2*!!(predicate)-1];
#define ASSERT(predicate) __ASSERT(predicate, __LINE__)
#endif

#define SCALE (1 << BIT_PRECISION)
#define PI 3.14159265
ASSERT(IS_POWER_OF_2(SAMPLES))
ASSERT(SAMPLES < UINT16_MAX)
static const uint8_t bitReverseTable[] =
{
    0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 0x10, 0x90, 0x50, 0xD0, 0x30, 
    0xB0, 0x70, 0xF0, 0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, 0x18, 0x98,
    0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8, 0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64,
    0xE4, 0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4, 0x0C, 0x8C, 0x4C, 0xCC,
    0x2C, 0xAC, 0x6C, 0xEC, 0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC, 0x02,
    0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2,
    0x72, 0xF2, 0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 0x1A, 0x9A, 0x5A,
    0xDA, 0x3A, 0xBA, 0x7A, 0xFA, 0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6,
    0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6, 0x0E, 0x8E, 0x4E, 0xCE, 0x2E,
    0xAE, 0x6E, 0xEE, 0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE, 0x01, 0x81,
    0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, 0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71,
    0xF1, 0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 0x19, 0x99, 0x59, 0xD9,
    0x39, 0xB9, 0x79, 0xF9, 0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, 0x15,
    0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5, 0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD,
    0x6D, 0xED, 0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD, 0x03, 0x83, 0x43,
    0xC3, 0x23, 0xA3, 0x63, 0xE3, 0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3,
    0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, 0x1B, 0x9B, 0x5B, 0xDB, 0x3B,
    0xBB, 0x7B, 0xFB, 0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, 0x17, 0x97,
    0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7, 0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F,
    0xEF, 0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF
};

static complex_t twiddleFactors[SAMPLES];

//static declarations
static inline void butterfly(complex_t *a, complex_t *b, complex_t twiddleFactor);
static inline uint16_t bitReverse(uint16_t value, uint8_t bitLength);
static inline uint8_t binaryLength(uint16_t value);
static inline uint8_t bitReversePartOfByte(uint8_t value, uint8_t numOfBits);
static inline complex_t multiply(complex_t a, complex_t b);
static inline complex_t sub(complex_t a, complex_t b);
static inline complex_t add(complex_t a, complex_t b);
static inline complex_t getTwiddleFactor(size_t n, size_t N, size_t size);
static inline uint16_t getBit(uint8_t bitNum, uint16_t value);
//

//complex math
static inline complex_t add(complex_t a, complex_t b)
{
    return (complex_t){.re = (a.re + b.re),
                       .im = (a.im + b.im)};
}

static inline complex_t sub(complex_t a, complex_t b)
{
    return (complex_t){.re = (a.re - b.re),  
                       .im = (a.im - b.im)};
}

static inline complex_t multiply(complex_t a, complex_t b)
{
    return (complex_t){.re = ((a.re * b.re) - (a.im * b.im)) / SCALE, 
                       .im = ((a.re * b.im) + (a.im * b.re)) / SCALE};
}

static inline complex_t getTwiddleFactor(size_t n, size_t N, size_t size)
{
    return twiddleFactors[(n * size) / N];
}
//

static inline uint16_t getBit(uint8_t bitNum, uint16_t value)
{
    return value & (uint16_t)(1 << bitNum);
}

static inline uint8_t bitReversePartOfByte(uint8_t value, uint8_t numOfBits)
{
    return (uint8_t)(bitReverseTable[value] >> (8 - numOfBits));
}

static inline uint16_t bitReverse(uint16_t value, uint8_t bitLength)
{
    if(bitLength > 8)
    {
        return (uint16_t)((bitReverseTable[(uint8_t)value] << (bitLength - 8))
                | bitReversePartOfByte((uint8_t)(value >> 8), (uint8_t)(bitLength - 8)));
    }
    else
    {
        return (uint16_t)(bitReverseTable[(uint8_t)value] >> (8 - bitLength));
    }

}

static inline uint8_t binaryLength(uint16_t value)
{
    if(!IS_POWER_OF_2(value))
    {
        return 0;
    }

    for(uint8_t i = 0; i < 16; i++)
    {
        if(value & (1 << i))
        {
            return i;
        }
    }
    return 0;
}

static inline void butterfly(complex_t *a, complex_t *b, complex_t twiddleFactor)
{
#define COMPLEX_DIV(cmplx, divider) do{cmplx->re /= divider; \
                                       cmplx->im /= divider;}while(0)
    complex_t tempA = *a;
    *a = add(*a, multiply(twiddleFactor, *b));
    *b = sub(tempA, multiply(twiddleFactor, *b));
    COMPLEX_DIV(a, 2);
    COMPLEX_DIV(b, 2);
}

void FFT_generateTwiddleFactors(void)
{
    for(uint16_t t = 0; t < SAMPLES; t++)
    {
        twiddleFactors[t].re = (int32_t)(SCALE * cos((2 * PI * (double)t) / SAMPLES));
        twiddleFactors[t].im = (int32_t)(-SCALE * sin((2 * PI * (double)t) / SAMPLES));
    }
}

complex_t *FFT_bitReverseArray(uint16_t *inputArray, complex_t *outputArray, uint16_t size)
{
    uint8_t bitReverseLength = binaryLength(size);

    for(size_t i = 0; i < size; i++)
    {
        outputArray[i].re = inputArray[bitReverse((uint16_t)i, bitReverseLength)];
        outputArray[i].im = 0;
    }
    return outputArray;
}

uint16_t FFT_complexAbs(complex_t *input)
{
    return (uint16_t) (abs(input->re) + abs(input->im));
}

complex_t *FFT_complexFFT(complex_t *const data, size_t size)
{
    for(size_t N = 2; N <= size; N *= 2)
    {
        for(size_t i = 0; i < size; i += N)
        {
            for(size_t j = 0, n = 0; j < (N / 2); j++, n++)
            {
                butterfly(&data[i + j], &data[i + j + (N / 2)], 
                          getTwiddleFactor(n, N, size)); 
            }
        }
    }

    return data;
}

