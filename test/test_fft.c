#include "fft.h"
#include "unity.h"
#include <stddef.h>

#ifndef NELEMS 
#define NELEMS(a) (sizeof(a) / sizeof(a[0]))
#endif

#define PI 3.14159265
#define SCALE (1 << BIT_PRECISION)

void print_complex(complex_t a)
{
    printf("{.re = %d, .im = %d}\n", a.re, a.im);
}

void print_complex_spectrum(complex_t *spectrum, size_t size)
{
    printf("SPECTRUM\n");
    for(size_t i = 0; i < size; i++)
    {
       printf("[%ld]", i);
       print_complex(spectrum[i]);
       if(i == size / 2 - 1)
       {
           printf("\n");
       }
    }
}

// cppcheck-suppress unusedFunction 
void test_FFT_complexFFT_testSingleSinewave(void)
{
#define TEST_FREQUENCY 3

    uint16_t input[SAMPLES];
    complex_t spectrum[SAMPLES];
    //uint16_t moduleOfSpectrum[SAMPLES / 2];
    
    //generate input signal
    uint16_t dcOffset = SCALE / 2;
    for(uint16_t i = 0; i < NELEMS(input); i++)
    {
        input[i] = (uint16_t)(dcOffset + 
                   (SCALE / 2) * sin(2 * PI * ((double)(TEST_FREQUENCY * i) 
                                     / (NELEMS(input)))));
    }
    //

    FFT_complexFFT(FFT_bitReverseArray(input, spectrum, NELEMS(input)),NELEMS(input));

    TEST_ASSERT(dcOffset - 1 <= spectrum[0].re && spectrum[0].re <= dcOffset + 1);
    TEST_ASSERT(-1028 <= spectrum[3].im && spectrum[3].im <= -1020);
    
    size_t zeroComponents[] = {1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15};
    for(size_t i = 0; i < NELEMS(zeroComponents); i++)
    {
        TEST_ASSERT_EQUAL(0, spectrum[zeroComponents[i]].re);
        TEST_ASSERT_EQUAL(0, spectrum[zeroComponents[i]].im);
    }

}

// cppcheck-suppress unusedFunction 
void test_FFT_complexFFT_testDC(void)
{
#define TEST_FREQUENCY 3

    uint16_t input[SAMPLES];
    complex_t spectrum[SAMPLES];
    //uint16_t moduleOfSpectrum[SAMPLES / 2];
    
    //generate input signal
    uint16_t dc = SCALE / 2;
    for(uint16_t i = 0; i < NELEMS(input); i++)
    {
        input[i] = dc;
    }
    //

    FFT_complexFFT(FFT_bitReverseArray(input, spectrum, NELEMS(input)),NELEMS(input));
    TEST_ASSERT(dc - 1 <= spectrum[0].re && spectrum[0].re <= dc + 1);
     
    for(uint16_t i = 1; i < NELEMS(spectrum); i++)
    {
        TEST_ASSERT_EQUAL(0, spectrum[i].re);
        TEST_ASSERT_EQUAL(0, spectrum[i].im);
    }
}

// cppcheck-suppress unusedFunction 
void test_FFT_complexFFT_testSingleCosinewave(void)
{
#define TEST_FREQUENCY 3

    uint16_t input[SAMPLES];
    complex_t spectrum[SAMPLES];
    //uint16_t moduleOfSpectrum[SAMPLES / 2];
    
    //generate input signal
    uint16_t dcOffset = SCALE / 2;
    for(uint16_t i = 0; i < NELEMS(input); i++)
    {
        input[i] = (uint16_t)(dcOffset + 
                   (SCALE / 2) * cos(2 * PI * ((double)(TEST_FREQUENCY * i) 
                                     / (NELEMS(input)))));
    }
    //

    FFT_complexFFT(FFT_bitReverseArray(input, spectrum, NELEMS(input)),NELEMS(input));

    TEST_ASSERT(dcOffset - 1 <= spectrum[0].re && spectrum[0].re <= dcOffset + 1);
    TEST_ASSERT(1020 <= spectrum[TEST_FREQUENCY].re && spectrum[TEST_FREQUENCY].re <= 1028);
    
    size_t zeroComponents[] = {1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15};
    for(size_t i = 0; i < NELEMS(zeroComponents); i++)
    {
        TEST_ASSERT_EQUAL(0, spectrum[zeroComponents[i]].re);
        TEST_ASSERT_EQUAL(0, spectrum[zeroComponents[i]].im);
    }
}

// cppcheck-suppress unusedFunction 
void test_FFT_complexFFT_testSineAndCosinewave(void)
{
#define SINE_FREQUENCY 3
#define COSINE_FREQUENCY 6

    uint16_t input[SAMPLES];
    complex_t spectrum[SAMPLES];
    
    //generate input signal
    uint16_t dcOffset = SCALE / 2;
    for(uint16_t i = 0; i < NELEMS(input); i++)
    {
        input[i] = (uint16_t)(dcOffset + 
                   ((SCALE / 4) * cos(2 * PI * ((double)(COSINE_FREQUENCY * i) 
                                     / (NELEMS(input)))))
                   +
                   ((SCALE / 4) * sin(2 * PI * ((double)(SINE_FREQUENCY * i) 
                                      / (NELEMS(input)))))
                   );
    }
    //

    FFT_complexFFT(FFT_bitReverseArray(input, spectrum, NELEMS(input)),NELEMS(input));
    print_complex_spectrum(spectrum, NELEMS(spectrum)); 

    TEST_ASSERT(dcOffset - 1 <= spectrum[0].re && spectrum[0].re <= dcOffset + 1);
    TEST_ASSERT(510 <= spectrum[COSINE_FREQUENCY].re 
                && spectrum[COSINE_FREQUENCY].re <= 514);
    
    TEST_ASSERT(-514 <= spectrum[SINE_FREQUENCY].im
                && spectrum[SINE_FREQUENCY].im <= -510);
    size_t zeroComponents[] = {1, 2, 4, 5, 7, 8, 9, 11, 12, 14, 15};
    for(size_t i = 0; i < NELEMS(zeroComponents); i++)
    {
        TEST_ASSERT_EQUAL(0, spectrum[zeroComponents[i]].re);
        TEST_ASSERT_EQUAL(0, spectrum[zeroComponents[i]].im);
    }

}


// cppcheck-suppress unusedFunction 
void suiteSetUp(void)
{
    FFT_generateTwiddleFactors();
}

// cppcheck-suppress unusedFunction 
int suiteTearDown(int num_failures)
{
    (void)num_failures;
    return 0;
}

// cppcheck-suppress unusedFunction 
void setUp(void)
{

}

// cppcheck-suppress unusedFunction 
void tearDown(void)
{
}

